import React from "react";
import { QueryRenderer } from "react-relay";
import HomeContainer from "../../container/Home";
import environment from "../../Environment";
import graphql from "babel-plugin-relay/macro";

const Home = () => {
    return (
        <div>
            <QueryRenderer
                environment={environment}
                query={graphql`
                    query HomeGetCountriesQuery {
                        countryStats {
                            country
                            cases
                            todayCases
                            deaths
                            todayDeaths
                            recovered
                            active
                            critical
                            casesPerOneMillion
                            deathsPerOneMillion
                            countryCode
                            confirmed
                        }
                        worldStats {
                            country
                            countryCode
                            cases
                            todayCases
                            deaths
                            todayDeaths
                            recovered
                            active
                            critical
                            casesPerOneMillion
                            confirmed
                        }
                    }
                `}
                variables={{}}
                render={({ error, props }) => {
                    if (!props) return <>Loading</>;

                    return <HomeContainer {...props} />;
                }}
            />
        </div>
    );
};

export default Home;
