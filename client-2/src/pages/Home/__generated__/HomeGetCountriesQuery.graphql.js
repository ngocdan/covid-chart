/**
 * @generated SignedSource<<b36178393bbc9f4cdcba180b0c7b14c4>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type HomeGetCountriesQuery$variables = {||};
export type HomeGetCountriesQuery$data = {|
  +countryStats: $ReadOnlyArray<{|
    +country: ?string,
    +cases: ?number,
    +todayCases: ?number,
    +deaths: ?number,
    +todayDeaths: ?number,
    +recovered: ?number,
    +active: ?number,
    +critical: ?number,
    +casesPerOneMillion: ?number,
    +deathsPerOneMillion: ?number,
    +countryCode: ?string,
    +confirmed: ?number,
  |}>,
  +worldStats: {|
    +country: ?string,
    +countryCode: ?string,
    +cases: ?number,
    +todayCases: ?number,
    +deaths: ?number,
    +todayDeaths: ?number,
    +recovered: ?number,
    +active: ?number,
    +critical: ?number,
    +casesPerOneMillion: ?string,
    +confirmed: ?number,
  |},
|};
export type HomeGetCountriesQuery = {|
  variables: HomeGetCountriesQuery$variables,
  response: HomeGetCountriesQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "country",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "cases",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "todayCases",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "deaths",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "todayDeaths",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "recovered",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "active",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "critical",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "casesPerOneMillion",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "countryCode",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "confirmed",
  "storageKey": null
},
v11 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "CountryStats",
    "kind": "LinkedField",
    "name": "countryStats",
    "plural": true,
    "selections": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/),
      (v6/*: any*/),
      (v7/*: any*/),
      (v8/*: any*/),
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "deathsPerOneMillion",
        "storageKey": null
      },
      (v9/*: any*/),
      (v10/*: any*/)
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "WorldStats",
    "kind": "LinkedField",
    "name": "worldStats",
    "plural": false,
    "selections": [
      (v0/*: any*/),
      (v9/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/),
      (v6/*: any*/),
      (v7/*: any*/),
      (v8/*: any*/),
      (v10/*: any*/)
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "HomeGetCountriesQuery",
    "selections": (v11/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "HomeGetCountriesQuery",
    "selections": (v11/*: any*/)
  },
  "params": {
    "cacheID": "ab6fd586d9780ecde97c0680c70fec4d",
    "id": null,
    "metadata": {},
    "name": "HomeGetCountriesQuery",
    "operationKind": "query",
    "text": "query HomeGetCountriesQuery {\n  countryStats {\n    country\n    cases\n    todayCases\n    deaths\n    todayDeaths\n    recovered\n    active\n    critical\n    casesPerOneMillion\n    deathsPerOneMillion\n    countryCode\n    confirmed\n  }\n  worldStats {\n    country\n    countryCode\n    cases\n    todayCases\n    deaths\n    todayDeaths\n    recovered\n    active\n    critical\n    casesPerOneMillion\n    confirmed\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "21726880842c66c7f5692ca9c899f3ca";

module.exports = ((node/*: any*/)/*: Query<
  HomeGetCountriesQuery$variables,
  HomeGetCountriesQuery$data,
>*/);
