import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import Chart from "react-apexcharts";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 250,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const HomeLayout = ({ country, handleChange, countries, dataChart }) => {
    const classes = useStyles();

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">
                    Tổng quan
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={country}
                    onChange={(e) => handleChange(e)}
                    label="Country"
                >
                    <MenuItem value="World">Thế giới</MenuItem>
                    {countries?.map((item, i) => (
                        <MenuItem
                            value={item.countryCode}
                            key={`countries-${i}`}
                        >
                            {item.country}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            {Object.keys(dataChart).length && (
                <Chart
                    options={dataChart?.options}
                    series={dataChart?.series}
                    type="pie"
                    width="800"
                />
            )}
        </div>
    );
};

export default HomeLayout;
