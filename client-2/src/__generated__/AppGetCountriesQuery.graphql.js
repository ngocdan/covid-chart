/**
 * @generated SignedSource<<f34fc188788e4679a6fd429a182c71b6>>
 * @flow
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest, Query } from 'relay-runtime';
export type AppGetCountriesQuery$variables = {||};
export type AppGetCountriesQuery$data = {|
  +countryStats: $ReadOnlyArray<{|
    +country: ?string,
    +cases: ?number,
    +todayCases: ?number,
    +deaths: ?number,
    +todayDeaths: ?number,
    +recovered: ?number,
    +active: ?number,
    +critical: ?number,
    +casesPerOneMillion: ?number,
    +deathsPerOneMillion: ?number,
    +countryCode: ?string,
    +confirmed: ?number,
  |}>,
|};
export type AppGetCountriesQuery = {|
  variables: AppGetCountriesQuery$variables,
  response: AppGetCountriesQuery$data,
|};
*/

var node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "CountryStats",
    "kind": "LinkedField",
    "name": "countryStats",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "country",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "cases",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "todayCases",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "deaths",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "todayDeaths",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "recovered",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "active",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "critical",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "casesPerOneMillion",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "deathsPerOneMillion",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "countryCode",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "confirmed",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "AppGetCountriesQuery",
    "selections": (v0/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "AppGetCountriesQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "0d36acf9ed407d9609fd3dc0a6e3956e",
    "id": null,
    "metadata": {},
    "name": "AppGetCountriesQuery",
    "operationKind": "query",
    "text": "query AppGetCountriesQuery {\n  countryStats {\n    country\n    cases\n    todayCases\n    deaths\n    todayDeaths\n    recovered\n    active\n    critical\n    casesPerOneMillion\n    deathsPerOneMillion\n    countryCode\n    confirmed\n  }\n}\n"
  }
};
})();

(node/*: any*/).hash = "7e1683d86740ab6cf0f5173f32ac14dd";

module.exports = ((node/*: any*/)/*: Query<
  AppGetCountriesQuery$variables,
  AppGetCountriesQuery$data,
>*/);
