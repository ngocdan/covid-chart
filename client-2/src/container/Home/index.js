import React, { useEffect, useState } from "react";
import HomeLayout from "../../components/Home";

const HomeContainer = ({ countryStats, worldStats }) => {
    const [countrySelect, setCountrySelect] = useState("World");
    const [dataChart, setDataChart] = useState({});

    const handleChange = (event) => {
        setCountrySelect(event.target.value);
    };

    const mappingDataChart = (data) => {
        return {
            options: {
                labels: [
                    "Tổng số ca mắc Covid-19",
                    "Số người hồi phục",
                    "Số người tử vong",
                ],
                theme: {
                    monochrome: {
                        enabled: false,
                    },
                },
                responsive: [
                    {
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: "100%",
                            },
                            legend: {
                                show: false,
                            },
                        },
                    },
                ],
            },
            series: [data.cases, data.recovered, data.deaths],
        };
    };

    const findCountry = (country) =>
        countryStats.find((x) => x.countryCode === country);

    useEffect(() => {
        const contryFound =
            countrySelect === "World" ? worldStats : findCountry(countrySelect);

        const data = mappingDataChart(contryFound);
        setDataChart(data);
    }, [countrySelect, worldStats]);

    return (
        <div>
            <div className="row">
                <div className="mixed-chart">
                    <HomeLayout
                        country={countrySelect}
                        handleChange={handleChange}
                        countries={countryStats}
                        dataChart={dataChart}
                    />
                </div>
            </div>
        </div>
    );
};

export default HomeContainer;
